# -*- encoding: utf-8 -*-
from django.contrib.auth.models import User
from models import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import *
from django.shortcuts import get_object_or_404,redirect, render, render_to_response
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from forms import *
from django.utils import timezone

def entires_view(request, username):
    try:
        usr = User.objects.get(username=username)
        entries = Entry.objects.filter(author = usr.pk).order_by('-date')
        return render(request, 'blog/userentries.html', {
            'entries': entries,
            'author': username,
            })
    except Exception as ex:
        messages.add_message(request,messages.INFO, ex)
        return redirect('home')
def blog_signup(request):

    if(request.method == 'POST'):
        try:
            form = UserCreationForm(request.POST)
            if form.is_valid():
                username = form.clean_username()
                password = form.clean_password2()
                email = form.cleaned_data.get('email')
                user = User.objects.create_user(username=username, password=password, email=email)
                user.full_clean()
                user.save()
                message = 'Congratulations! You have successfully registered'
                messages.add_message(request, messages.INFO, message)
                return redirect('home')
        except Exception as ex :
            messages.add_message(request, messages.INFO, ex)
            return redirect('blog_signup')
    else:
        form = UserCreationForm()
        return render(request, 'blog/signup1.html', locals())

def blog_login(request):
    if(request.method == 'POST'):
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            if user is not None:
                messages.add_message(request, messages.INFO, 'You have been successfully logged in' )
                login(request, user)
                return redirect('home')
            else:
                messages.add_message(request, messages.INFO, 'Invalid username or password' )
                return redirect('blog_login')
    else:
        form = AuthenticationForm()
        return render(request, 'blog/login.html', locals())

def blog_logout(request):
    if request.user.is_authenticated():
        logout(request)
        messages.add_message(request, messages.INFO, 'You have been successfully logged out' )
    else:
        pass
    return redirect('home')
def contact(request):
    try:
        if not request.user.is_authenticated(): raise PermissionDenied
        author = User.objects.get(username=request.user)
        if request.method == 'POST': # formularz został przesłany
            form = EntryForm(request.POST) # powiązanie formularza z przesłanymi danymi
            if form.is_valid():
                title = form.cleaned_data.get('title')
                text = form.cleaned_data.get('text')
                tags = form.cleaned_data.get('tags')

                author = User.objects.get(username=request.user)

                entry = Entry(title=title,text=text,author=author,lastmodified=timezone.now())

                entry.full_clean()
                entry.save()
                for tag in tags:
                    tg = Tags.objects.get(pk=tag)
                    entry.tags.add(tg)
                return redirect('home') # przekierowanie !!!
        else:
            form = EntryForm(initial={
                'text': u'Ta strona jest świetna!',
            }) # nowy formularz
        return render(request,'blog/add1.html', locals())
    except PermissionDenied:
        messages.add_message(request, messages.INFO, 'You have no permission to add entry')
        return redirect('home')
def blog_edit(request, pk):
    try:
        if not request.user.is_authenticated(): raise Exception('You have no permission to edit entry')
        entry = Entry.objects.get(pk=pk)
        if not in_mod_group(request.user):
            if request.user != entry.author : raise Exception('Nie twój post!')
            if (timezone.now() - entry.lastmodified).seconds > 600  : raise Exception('Mineło 10 minut')
        if request.method == 'POST':
           form = EntryForm(request.POST)
           if form.is_valid():
               title = form.cleaned_data.get('title')
               text = form.cleaned_data.get('text')
               tags = form.cleaned_data.get('tags')
               lastmodifiedby = User.objects.get(username=request.user)
               entry.title = title
               entry.text = text
               entry.lasteditby = unicode(lastmodifiedby.username)
               entry.lastmodified = timezone.now()
               entry.tags = []
               entry.save()
               for tag in tags:
                   tg = Tags.objects.get(pk = tag)
                   entry.tags.add(tg)
               messages.add_message(request, messages.INFO, 'Successfully edited!')
               return redirect('home')
           else:
               messages.add_message(request, messages.INFO, 'cos nie edited!')
               return  redirect('home')
        else:
            lista = []
            for t in entry.tags.all():
                lista.append(t.pk)
            form = EntryForm(initial={'title':entry.title,'text':entry.text, 'tags':lista})
            return render(request,'blog/edit.html', locals())
    except Exception as ex:
        messages.add_message(request, messages.INFO, ex)
        return redirect('home')
def in_mod_group(user):
    if user:
        if user.groups.filter(name='mod').count() == 0 :
            return False
        else:
            return True
    else:
        return False