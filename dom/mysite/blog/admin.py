from models import *
from django.contrib import admin
from forms import *
admin.site.register(Tags)



class EntryAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'date', 'tag')
    list_filter = ('date', )
    ordering = ('date', )
    search_fields = ('tags__name',)

admin.site.register(Entry, EntryAdmin)